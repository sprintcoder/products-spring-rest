# README #

## What is this repository for? ##

This is a simple REST API for creating, reading and updating products

## How do I get set up? ##

### Database setup ###

For getting database up and running you will have to install docker and docker compose.<br/><br/>
Go to src/main/docker folder and run 'docker-compose up' which will create dev and test databases.

### Running the project ###

After importing the project into your IDE of choice just
run main function in ProductsApplication class which will migrate 
and setup database tables and run the application on localhost 8080 port.

### Running tests and building project ###

Run mvn clean install