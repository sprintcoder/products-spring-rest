package hr.ingemark.products.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import hr.ingemark.products.domain.Product;
import hr.ingemark.products.dto.ProductRequest;
import hr.ingemark.products.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WireMockTest(httpPort = 9091)
@Transactional
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductRepository productRepository;

    @Test
    void shouldCreateProduct() throws Exception {
        // GIVEN
        String code = "1212121212";
        ProductRequest productRequest = new ProductRequest();
        productRequest.setCode(code);
        productRequest.setName("Bottle of wine");
        productRequest.setPriceHrk(new BigDecimal("22.5"));
        productRequest.setDescription("Old Wine");
        productRequest.setIsAvailable(true);
        stubFor(get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("response.json")));

        // WHEN
        ResultActions resultActions = mockMvc.perform(post("/api/v1/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isCreated());

        // THEN
        Product product = productRepository.findByCode(code).get();
        resultActions
                .andExpect(jsonPath("$.id").value(product.getId()))
                .andExpect(jsonPath("$.code").value(code))
                .andExpect(jsonPath("$.name").value(productRequest.getName()))
                .andExpect(jsonPath("$.priceHrk").value(productRequest.getPriceHrk()))
                .andExpect(jsonPath("$.priceEur").value(2.98))
                .andExpect(jsonPath("$.description").value(productRequest.getDescription()))
                .andExpect(jsonPath("$.isAvailable").value(productRequest.getIsAvailable()));

        assertThat(product.getId()).isNotNull();
        assertThat(product.getCode()).isEqualTo(code);
        assertThat(product.getName()).isEqualTo(productRequest.getName());
        assertThat(product.getPriceHrk()).isEqualTo(productRequest.getPriceHrk());
        assertThat(product.getPriceEur()).isEqualTo(new BigDecimal("2.98"));
        assertThat(product.getDescription()).isEqualTo(productRequest.getDescription());
        assertThat(product.getIsAvailable()).isEqualTo(productRequest.getIsAvailable());
    }

    @Test
    void shouldGetConstraintViolations() throws Exception {
        // GIVEN
        String code = "1212121212";
        Product product = new Product();
        product.setCode(code);
        product.setName("Bottle of wine");
        product.setPriceHrk(new BigDecimal("22.5"));
        product.setPriceEur(new BigDecimal("2.98"));
        product.setDescription("Old Wine");
        product.setIsAvailable(true);

        productRepository.save(product);

        ProductRequest productRequest = new ProductRequest();
        productRequest.setCode(code);
        productRequest.setName("");
        productRequest.setPriceHrk(new BigDecimal("-22.5"));
        productRequest.setDescription("Old Wine");
        productRequest.setIsAvailable(null);
        stubFor(get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("response.json")));

        // WHEN
        ResultActions resultActions = mockMvc.perform(post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isBadRequest());

        // THEN
        resultActions
                .andExpect(jsonPath("$.status").value(400))
                .andExpect(jsonPath("$.violations[0].field").value("code"))
                .andExpect(jsonPath("$.violations[0].message").value("Product with this code already exists."))
                .andExpect(jsonPath("$.violations[1].field").value("isAvailable"))
                .andExpect(jsonPath("$.violations[1].message").value("must not be null"))
                .andExpect(jsonPath("$.violations[2].field").value("name"))
                .andExpect(jsonPath("$.violations[2].message").value("must not be empty"))
                .andExpect(jsonPath("$.violations[3].field").value("priceHrk"))
                .andExpect(jsonPath("$.violations[3].message").value("must be greater than or equal to 0"))
                .andExpect(jsonPath("$.title").value("Constraint Violation"));
    }

    @Test
    void shouldFindAllProducts() throws Exception {
        // GIVEN
        Product productWine = new Product();
        productWine.setCode("1212121212");
        productWine.setName("Bottle of wine");
        productWine.setPriceHrk(new BigDecimal("22.5"));
        productWine.setPriceEur(new BigDecimal("2.98"));
        productWine.setDescription("Old Wine");
        productWine.setIsAvailable(true);
        productRepository.save(productWine);

        Product productBanana = new Product();
        productBanana.setCode("1212121213");
        productBanana.setName("Banana");
        productBanana.setPriceHrk(new BigDecimal("6.45"));
        productBanana.setPriceEur(new BigDecimal("0.85"));
        productBanana.setDescription("Fresh Banana");
        productBanana.setIsAvailable(true);
        productRepository.save(productBanana);

        // WHEN
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products"))
                .andExpect(status().isOk());

        // THEN
        resultActions
                .andExpect(jsonPath("$._embedded.productModelList[0].id").value(productWine.getId()))
                .andExpect(jsonPath("$._embedded.productModelList[0].code").value(productWine.getCode()))
                .andExpect(jsonPath("$._embedded.productModelList[0].name").value(productWine.getName()))
                .andExpect(jsonPath("$._embedded.productModelList[0].priceHrk").value(productWine.getPriceHrk()))
                .andExpect(jsonPath("$._embedded.productModelList[0].priceEur").value(productWine.getPriceEur()))
                .andExpect(jsonPath("$._embedded.productModelList[0].description").value(productWine.getDescription()))
                .andExpect(jsonPath("$._embedded.productModelList[0].isAvailable").value(productWine.getIsAvailable()))
                .andExpect(jsonPath("$._embedded.productModelList[1].id").value(productBanana.getId()))
                .andExpect(jsonPath("$._embedded.productModelList[1].code").value(productBanana.getCode()))
                .andExpect(jsonPath("$._embedded.productModelList[1].name").value(productBanana.getName()))
                .andExpect(jsonPath("$._embedded.productModelList[1].priceHrk").value(productBanana.getPriceHrk()))
                .andExpect(jsonPath("$._embedded.productModelList[1].priceEur").value(productBanana.getPriceEur()))
                .andExpect(jsonPath("$._embedded.productModelList[1].description").value(productBanana.getDescription()))
                .andExpect(jsonPath("$._embedded.productModelList[1].isAvailable").value(productBanana.getIsAvailable()))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/v1/products?page=0&size=20"))
                .andExpect(jsonPath("$.page.size").value(20))
                .andExpect(jsonPath("$.page.totalElements").value(2))
                .andExpect(jsonPath("$.page.totalPages").value(1))
                .andExpect(jsonPath("$.page.number").value(0));

    }

    @Test
    void shouldFindAllProductsPaginated() throws Exception {
        // GIVEN
        Product productWine = new Product();
        productWine.setCode("1212121212");
        productWine.setName("Bottle of wine");
        productWine.setPriceHrk(new BigDecimal("22.5"));
        productWine.setPriceEur(new BigDecimal("2.98"));
        productWine.setDescription("Old Wine");
        productWine.setIsAvailable(true);
        productRepository.save(productWine);

        Product productBanana = new Product();
        productBanana.setCode("1212121213");
        productBanana.setName("Banana");
        productBanana.setPriceHrk(new BigDecimal("6.45"));
        productBanana.setPriceEur(new BigDecimal("0.85"));
        productBanana.setDescription("Fresh Banana");
        productBanana.setIsAvailable(true);
        productRepository.save(productBanana);

        // WHEN
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products")
                        .param("page", "1")
                        .param("size", "1"))
                .andExpect(status().isOk());

        // THEN
        resultActions
                .andExpect(jsonPath("$._embedded.productModelList[0].id").value(productBanana.getId()))
                .andExpect(jsonPath("$._embedded.productModelList[0].code").value(productBanana.getCode()))
                .andExpect(jsonPath("$._embedded.productModelList[0].name").value(productBanana.getName()))
                .andExpect(jsonPath("$._embedded.productModelList[0].priceHrk").value(productBanana.getPriceHrk()))
                .andExpect(jsonPath("$._embedded.productModelList[0].priceEur").value(productBanana.getPriceEur()))
                .andExpect(jsonPath("$._embedded.productModelList[0].description").value(productBanana.getDescription()))
                .andExpect(jsonPath("$._embedded.productModelList[0].isAvailable").value(productBanana.getIsAvailable()))
                .andExpect(jsonPath("$._links.first.href").value("http://localhost/api/v1/products?page=0&size=1"))
                .andExpect(jsonPath("$._links.prev.href").value("http://localhost/api/v1/products?page=0&size=1"))
                .andExpect(jsonPath("$._links.self.href").value("http://localhost/api/v1/products?page=1&size=1"))
                .andExpect(jsonPath("$._links.last.href").value("http://localhost/api/v1/products?page=1&size=1"))
                .andExpect(jsonPath("$.page.size").value(1))
                .andExpect(jsonPath("$.page.totalElements").value(2))
                .andExpect(jsonPath("$.page.totalPages").value(2))
                .andExpect(jsonPath("$.page.number").value(1));

    }

    @Test
    void shouldFindOneProduct() throws Exception {
        // GIVEN
        Product productWine = new Product();
        productWine.setCode("1212121212");
        productWine.setName("Bottle of wine");
        productWine.setPriceHrk(new BigDecimal("22.5"));
        productWine.setPriceEur(new BigDecimal("2.98"));
        productWine.setDescription("Old Wine");
        productWine.setIsAvailable(true);
        productRepository.save(productWine);

        // WHEN
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/" + productWine.getId()))
                .andExpect(status().isOk());

        //THEN
        resultActions
                .andExpect(jsonPath("$.id").value(productWine.getId()))
                .andExpect(jsonPath("$.code").value(productWine.getCode()))
                .andExpect(jsonPath("$.name").value(productWine.getName()))
                .andExpect(jsonPath("$.priceHrk").value(productWine.getPriceHrk()))
                .andExpect(jsonPath("$.priceEur").value(productWine.getPriceEur()))
                .andExpect(jsonPath("$.description").value(productWine.getDescription()))
                .andExpect(jsonPath("$.isAvailable").value(productWine.getIsAvailable()));
    }

    @Test
    void shouldGetNotFoundWhenGetNonExistingProduct() throws Exception {
        // GIVEN
        Product productWine = new Product();
        productWine.setCode("1212121212");
        productWine.setName("Bottle of wine");
        productWine.setPriceHrk(new BigDecimal("22.5"));
        productWine.setPriceEur(new BigDecimal("2.98"));
        productWine.setDescription("Old Wine");
        productWine.setIsAvailable(true);
        productRepository.save(productWine);

        // WHEN
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/" + Long.MAX_VALUE))
                .andExpect(status().isNotFound());

        //THEN
        resultActions
                .andExpect(jsonPath("$.title").value("Not found"))
                .andExpect(jsonPath("$.status").value(404));
    }

    @Test
    void shouldUpdateProduct() throws Exception {
        // GIVEN
        String code = "1212121212";
        ProductRequest productRequest = new ProductRequest();
        productRequest.setCode(code);
        productRequest.setName("Bottle of wine");
        productRequest.setPriceHrk(new BigDecimal("22.5"));
        productRequest.setDescription("Old Wine");
        productRequest.setIsAvailable(true);
        stubFor(get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("response.json")));

        Product productBanana = new Product();
        productBanana.setCode("1212121213");
        productBanana.setName("Banana");
        productBanana.setPriceHrk(new BigDecimal("6.45"));
        productBanana.setPriceEur(new BigDecimal("0.85"));
        productBanana.setDescription("Fresh Banana");
        productBanana.setIsAvailable(true);
        productRepository.save(productBanana);

        // WHEN
        ResultActions resultActions = mockMvc.perform(put("/api/v1/products/" + productBanana.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isOk());

        // THEN
        Product product = productRepository.findByCode(code).get();
        resultActions
                .andExpect(jsonPath("$.id").value(product.getId()))
                .andExpect(jsonPath("$.code").value(code))
                .andExpect(jsonPath("$.name").value(productRequest.getName()))
                .andExpect(jsonPath("$.priceHrk").value(productRequest.getPriceHrk()))
                .andExpect(jsonPath("$.priceEur").value(2.98))
                .andExpect(jsonPath("$.description").value(productRequest.getDescription()))
                .andExpect(jsonPath("$.isAvailable").value(productRequest.getIsAvailable()));

        assertThat(product.getId()).isEqualTo(productBanana.getId());
        assertThat(product.getCode()).isEqualTo(code);
        assertThat(product.getName()).isEqualTo(productRequest.getName());
        assertThat(product.getPriceHrk()).isEqualTo(productRequest.getPriceHrk());
        assertThat(product.getPriceEur()).isEqualTo(new BigDecimal("2.98"));
        assertThat(product.getDescription()).isEqualTo(productRequest.getDescription());
        assertThat(product.getIsAvailable()).isEqualTo(productRequest.getIsAvailable());
    }

    @Test
    void shouldGetNotFoundOnUpdatingNonExistingProduct() throws Exception {
        // GIVEN
        String code = "1212121212";
        ProductRequest productRequest = new ProductRequest();
        productRequest.setCode(code);
        productRequest.setName("Bottle of wine");
        productRequest.setPriceHrk(new BigDecimal("22.5"));
        productRequest.setDescription("Old Wine");
        productRequest.setIsAvailable(true);
        stubFor(get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("response.json")));

        Product productBanana = new Product();
        productBanana.setCode("1212121213");
        productBanana.setName("Banana");
        productBanana.setPriceHrk(new BigDecimal("6.45"));
        productBanana.setPriceEur(new BigDecimal("0.85"));
        productBanana.setDescription("Fresh Banana");
        productBanana.setIsAvailable(true);
        productRepository.save(productBanana);

        // WHEN
        mockMvc.perform(put("/api/v1/products/" + Long.MAX_VALUE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldGetConstraintViolationOnUpdatingProduct() throws Exception {
        // GIVEN
        String code = "12121212121212";
        ProductRequest productRequest = new ProductRequest();
        productRequest.setCode(code);
        productRequest.setName("Bottle of wine");
        productRequest.setPriceHrk(new BigDecimal("22.5678"));
        productRequest.setDescription("Old Wine");
        productRequest.setIsAvailable(true);
        stubFor(get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("response.json")));

        Product productBanana = new Product();
        productBanana.setCode("1212121213");
        productBanana.setName("Banana");
        productBanana.setPriceHrk(new BigDecimal("6.45"));
        productBanana.setPriceEur(new BigDecimal("0.85"));
        productBanana.setDescription("Fresh Banana");
        productBanana.setIsAvailable(true);
        productRepository.save(productBanana);

        // WHEN
        mockMvc.perform(put("/api/v1/products/" + productBanana.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldDeleteProduct() throws Exception {
        Product productBanana = new Product();
        productBanana.setCode("1212121213");
        productBanana.setName("Banana");
        productBanana.setPriceHrk(new BigDecimal("6.45"));
        productBanana.setPriceEur(new BigDecimal("0.85"));
        productBanana.setDescription("Fresh Banana");
        productBanana.setIsAvailable(true);
        productRepository.save(productBanana);

        // WHEN
        mockMvc.perform(delete("/api/v1/products/" + productBanana.getId()))
                .andExpect(status().isNoContent());

        //THEN
        Optional<Product> productExpected = productRepository.findById(productBanana.getId());
        assertThat(productExpected.isEmpty()).isTrue();
    }

    @Test
    void shouldGetNotFoundOnDeleteProduct() throws Exception {
        Product productBanana = new Product();
        productBanana.setCode("1212121213");
        productBanana.setName("Banana");
        productBanana.setPriceHrk(new BigDecimal("6.45"));
        productBanana.setPriceEur(new BigDecimal("0.85"));
        productBanana.setDescription("Fresh Banana");
        productBanana.setIsAvailable(true);
        productRepository.save(productBanana);

        // WHEN
        mockMvc.perform(delete("/api/v1/products/" + Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldGetServiceUnavailableWhenServiceReturnEmptyList() throws Exception {
        // GIVEN
        String code = "1212121212";
        ProductRequest productRequest = new ProductRequest();
        productRequest.setCode(code);
        productRequest.setName("Bottle of wine");
        productRequest.setPriceHrk(new BigDecimal("22.5"));
        productRequest.setDescription("Old Wine");
        productRequest.setIsAvailable(true);
        stubFor(get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("response-empty.json")));

        // WHEN
        ResultActions resultActions = mockMvc.perform(post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isServiceUnavailable());

        // THEN
        resultActions
                .andExpect(jsonPath("$.title").value("Not available"))
                .andExpect(jsonPath("$.status").value(503))
                .andExpect(jsonPath("$.detail").value("Hnb service is currently unavailable"));
    }

    @Test
    void shouldGetServiceUnavailableWhenService4xxError() throws Exception {
        // GIVEN
        String code = "1212121212";
        ProductRequest productRequest = new ProductRequest();
        productRequest.setCode(code);
        productRequest.setName("Bottle of wine");
        productRequest.setPriceHrk(new BigDecimal("22.5"));
        productRequest.setDescription("Old Wine");
        productRequest.setIsAvailable(true);
        stubFor(get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withStatus(400)));

        // WHEN
        ResultActions resultActions = mockMvc.perform(post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isServiceUnavailable());

        resultActions
                .andExpect(jsonPath("$.title").value("Not available"))
                .andExpect(jsonPath("$.status").value(503))
                .andExpect(jsonPath("$.detail").value("Hnb service is currently unavailable"));
    }

    @Test
    void shouldGetServiceUnavailableWhenService5xxError() throws Exception {
        // GIVEN
        String code = "1212121212";
        ProductRequest productRequest = new ProductRequest();
        productRequest.setCode(code);
        productRequest.setName("Bottle of wine");
        productRequest.setPriceHrk(new BigDecimal("22.5"));
        productRequest.setDescription("Old Wine");
        productRequest.setIsAvailable(true);
        stubFor(get(urlEqualTo("/tecajn/v2?valuta=EUR"))
                .willReturn(aResponse()
                        .withStatus(500)));

        // WHEN
        ResultActions resultActions = mockMvc.perform(post("/api/v1/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(productRequest)))
                .andExpect(status().isServiceUnavailable());

        resultActions
                .andExpect(jsonPath("$.title").value("Not available"))
                .andExpect(jsonPath("$.status").value(503))
                .andExpect(jsonPath("$.detail").value("Hnb service is currently unavailable"));
    }

}