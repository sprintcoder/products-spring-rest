CREATE TABLE product(
    id bigserial PRIMARY KEY,
    code VARCHAR(10) UNIQUE NOT NULL,
    name VARCHAR(200) NOT NULL,
    price_hrk NUMERIC(19,2) NOT NULL,
    price_eur NUMERIC(19,2) NOT NULL,
    description TEXT,
    is_available BOOLEAN NOT NULL
)