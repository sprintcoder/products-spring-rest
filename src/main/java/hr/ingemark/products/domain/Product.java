package hr.ingemark.products.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 10, unique = true, nullable = false)
    private String code;

    @Column(length = 200, nullable = false)
    private String name;

    @Column(nullable = false, precision = 19, scale = 2)
    private BigDecimal priceHrk;

    @Column(nullable = false, precision = 19, scale = 2)
    private BigDecimal priceEur;

    private String description;

    @Column(nullable = false)
    private Boolean isAvailable;

}
