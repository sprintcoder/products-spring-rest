package hr.ingemark.products.controller;

import hr.ingemark.products.dto.ProductRequest;
import hr.ingemark.products.dto.ProductResponse;
import hr.ingemark.products.model.ProductModel;
import hr.ingemark.products.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public ResponseEntity<ProductResponse> create(@Valid @RequestBody ProductRequest productRequest) throws ParseException, URISyntaxException {
        ProductResponse productResponse = productService.create(productRequest);
        return ResponseEntity
                .created(new URI("/api/v1/products/" + productResponse.getId()))
                .body(productResponse);
    }

    @GetMapping
    public PagedModel<ProductModel> findAll(Pageable pageable) {
        return productService.findAll(pageable);
    }

    @GetMapping("/{id}")
    public ProductResponse findOne(@PathVariable Long id) {
        return productService.findById(id);
    }

    @PutMapping("/{id}")
    public ProductResponse update(@Valid @RequestBody ProductRequest productRequest, @PathVariable Long id) throws ParseException {
        return productService.update(productRequest, id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        productService.deleteById(id);
        return ResponseEntity
                .noContent()
                .build();
    }

}
