package hr.ingemark.products.service;

import hr.ingemark.products.assembler.ProductModelAssembler;
import hr.ingemark.products.domain.Product;
import hr.ingemark.products.dto.ProductRequest;
import hr.ingemark.products.dto.ProductResponse;
import hr.ingemark.products.error.ProductNotFoundProblem;
import hr.ingemark.products.mapper.ProductMapper;
import hr.ingemark.products.model.ProductModel;
import hr.ingemark.products.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final PagedResourcesAssembler<Product> pagedResourcesAssembler;
    private final ProductModelAssembler productModelAssembler;

    @Override
    public ProductResponse create(ProductRequest productRequest) throws ParseException {
        Product product = productRepository.save(productMapper.toProduct(productRequest, new Product()));
        return productMapper.toProductResponse(product);
    }

    @Override
    public PagedModel<ProductModel> findAll(Pageable pageable) {
        Page<Product> products = productRepository.findAll(pageable);
        return pagedResourcesAssembler.toModel(products, productModelAssembler);
    }

    @Override
    public ProductResponse findById(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundProblem(id));
        return productMapper.toProductResponse(product);
    }

    @Override
    public ProductResponse update(ProductRequest productRequest, Long id) throws ParseException {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundProblem(id));
        Product productUpdated = productMapper.toProduct(productRequest, product);
        productRepository.save(productUpdated);
        return productMapper.toProductResponse(productUpdated);
    }

    @Override
    public void deleteById(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundProblem(id));
        productRepository.delete(product);
    }

}
