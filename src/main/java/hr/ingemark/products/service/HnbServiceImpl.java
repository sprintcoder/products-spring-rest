package hr.ingemark.products.service;

import hr.ingemark.products.dto.CurrencyInfo;
import hr.ingemark.products.error.ServiceNotAvailableProblem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class HnbServiceImpl implements HnbService {

    @Value("${products.hnb-url}")
    private String hnbUrl;

    @Override
    public CurrencyInfo getCurrencyInfo() {
        WebClient webClient = WebClient.create(hnbUrl);
        List<CurrencyInfo> currencyInfos = webClient
                .get()
                .uri(uriBuilder -> uriBuilder.path("tecajn/v2")
                        .queryParam("valuta", "EUR")
                        .build()
                )
                .retrieve()
                .onStatus(HttpStatus::isError, response -> Mono.error(new ServiceNotAvailableProblem()))
                .bodyToMono(new ParameterizedTypeReference<List<CurrencyInfo>>() {})
                .block();
        if (CollectionUtils.isEmpty(currencyInfos)) {
            throw new ServiceNotAvailableProblem();
        }
        return currencyInfos.get(0);
    }
}
