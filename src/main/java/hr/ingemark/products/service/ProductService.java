package hr.ingemark.products.service;

import hr.ingemark.products.dto.ProductRequest;
import hr.ingemark.products.dto.ProductResponse;
import hr.ingemark.products.model.ProductModel;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;

import java.text.ParseException;

public interface ProductService {

    ProductResponse create(ProductRequest productRequest) throws ParseException;
    PagedModel<ProductModel> findAll(Pageable pageable);
    ProductResponse findById(Long id);
    ProductResponse update(ProductRequest productRequest, Long id) throws ParseException;
    void deleteById(Long id);

}
