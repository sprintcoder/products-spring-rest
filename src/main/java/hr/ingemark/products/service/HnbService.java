package hr.ingemark.products.service;

import hr.ingemark.products.dto.CurrencyInfo;

public interface HnbService {

    CurrencyInfo getCurrencyInfo();

}
