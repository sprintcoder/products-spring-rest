package hr.ingemark.products.model;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.math.BigDecimal;

@Data
public class ProductModel extends RepresentationModel<ProductModel> {

    private Long id;
    private String code;
    private String name;
    private BigDecimal priceHrk;
    private BigDecimal priceEur;
    private String description;
    private Boolean isAvailable;

}
