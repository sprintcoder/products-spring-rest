package hr.ingemark.products.mapper;

import hr.ingemark.products.domain.Product;
import hr.ingemark.products.dto.CurrencyInfo;
import hr.ingemark.products.dto.ProductRequest;
import hr.ingemark.products.dto.ProductResponse;
import hr.ingemark.products.service.HnbService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

@Component
@RequiredArgsConstructor
public class ProductMapper {

    private final HnbService hnbService;

    public Product toProduct(ProductRequest productRequest, Product product) throws ParseException {
        product.setCode(productRequest.getCode());
        product.setName(productRequest.getName());
        product.setPriceHrk(productRequest.getPriceHrk());

        CurrencyInfo currencyInfo = hnbService.getCurrencyInfo();
        DecimalFormat decimalFormat = new DecimalFormat();
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator(',');
        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        Number number = decimalFormat.parse(currencyInfo.getSrednjiTecaj());
        BigDecimal srednjiTecaj = BigDecimal.valueOf(number.doubleValue());
        product.setPriceEur(productRequest.getPriceHrk().divide(srednjiTecaj, 2, RoundingMode.HALF_EVEN));
        product.setDescription(productRequest.getDescription());
        product.setIsAvailable(productRequest.getIsAvailable());

        return product;
    }

    public ProductResponse toProductResponse(Product product) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setId(product.getId());
        productResponse.setCode(product.getCode());
        productResponse.setName(product.getName());
        productResponse.setPriceHrk(product.getPriceHrk());
        productResponse.setPriceEur(product.getPriceEur());
        productResponse.setDescription(product.getDescription());
        productResponse.setIsAvailable(product.getIsAvailable());
        return productResponse;
    }

}
