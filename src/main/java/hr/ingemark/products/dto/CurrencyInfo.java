package hr.ingemark.products.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CurrencyInfo {

    @JsonProperty("broj_tecajnice")
    private String brojTecajnice;
    @JsonProperty("datum_primjene")
    private LocalDate datumPrimjene;
    private String drzava;
    @JsonProperty("drzava_iso")
    private String drzavaIso;
    @JsonProperty("sifra_valute")
    private String sifraValute;
    private String valuta;
    private Integer jedinica;
    @JsonProperty("kupovni_tecaj")
    private String kupovniTecaj;
    @JsonProperty("srednji_tecaj")
    private String srednjiTecaj;
    @JsonProperty("prodajni_tecaj")
    private String prodajniTecaj;

}
