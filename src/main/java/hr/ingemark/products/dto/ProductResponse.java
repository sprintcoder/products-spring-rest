package hr.ingemark.products.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductResponse {

    private Long id;
    private String code;
    private String name;
    private BigDecimal priceHrk;
    private BigDecimal priceEur;
    private String Description;
    private Boolean isAvailable;

}
