package hr.ingemark.products.dto;

import hr.ingemark.products.error.CodeUniqueConstraint;
import lombok.Data;

import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
public class ProductRequest {

    @Size(min = 10, max = 10)
    @CodeUniqueConstraint
    private String code;

    @NotEmpty
    @Size(max = 200)
    private String name;

    @Digits(integer = 19, fraction = 2)
    @PositiveOrZero
    @NotNull
    private BigDecimal priceHrk;

    private String Description;

    @NotNull
    private Boolean isAvailable;

}
