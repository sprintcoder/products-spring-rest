package hr.ingemark.products.error;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CodeUniqueValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CodeUniqueConstraint {

    String message() default "Product with this code already exists.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
