package hr.ingemark.products.error;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class ProductNotFoundProblem extends AbstractThrowableProblem {

    public ProductNotFoundProblem(Long id) {
        super(
                URI.create("/not-found"),
                "Not found",
                Status.NOT_FOUND,
                String.format("Product '%s' not found", id)
        );
    }

}
