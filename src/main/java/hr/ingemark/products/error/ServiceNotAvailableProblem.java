package hr.ingemark.products.error;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

public class ServiceNotAvailableProblem extends AbstractThrowableProblem {

    public ServiceNotAvailableProblem() {
        super(
                URI.create("/not-available"),
                "Not available",
                Status.SERVICE_UNAVAILABLE,
                "Hnb service is currently unavailable"
        );
    }

}
