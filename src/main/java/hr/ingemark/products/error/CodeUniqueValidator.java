package hr.ingemark.products.error;

import hr.ingemark.products.repository.ProductRepository;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
public class CodeUniqueValidator implements ConstraintValidator<CodeUniqueConstraint, String> {

    private final ProductRepository productRepository;

    @Override
    public void initialize(CodeUniqueConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String code, ConstraintValidatorContext constraintValidatorContext) {
        return productRepository.findByCode(code).isEmpty();
    }
}
