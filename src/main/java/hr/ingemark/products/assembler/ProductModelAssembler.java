package hr.ingemark.products.assembler;

import hr.ingemark.products.controller.ProductController;
import hr.ingemark.products.domain.Product;
import hr.ingemark.products.model.ProductModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class ProductModelAssembler extends RepresentationModelAssemblerSupport<Product, ProductModel> {
    public ProductModelAssembler() {
        super(ProductController.class, ProductModel.class);
    }

    @Override
    public ProductModel toModel(Product entity) {
        ProductModel productModel = instantiateModel(entity);

        productModel.setId(entity.getId());
        productModel.setCode(entity.getCode());
        productModel.setName(entity.getName());
        productModel.setPriceHrk(entity.getPriceHrk());
        productModel.setPriceEur(entity.getPriceEur());
        productModel.setDescription(entity.getDescription());
        productModel.setIsAvailable(entity.getIsAvailable());

        return productModel;
    }
}
